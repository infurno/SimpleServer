var gulp = require('gulp');

var EXPRESS_PORT = 4000;
var EXPRESS_ROOT = './public/';


// Let's make things more readable by
// encapsulating each part's setup
// in its own method
function startExpress() {

  var express = require('express');
  var app = express();
  app.use(express.static(EXPRESS_ROOT));
  app.listen(EXPRESS_PORT);
}


// Default task that will be run
// when no parameter is provided
// to gulp
gulp.task('default', function () {

  startExpress();
  
  gulp.watch('*.html');
});